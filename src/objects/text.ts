import { ITextObjectOptions, TextObject } from '@yepreally/game-engine';
import { AsteroidsGame } from '../AsteroidsGame';
import { Ship } from './ship';

export type IAtariTextOptions = Pick<ITextObjectOptions, 'x' | 'y'> & Partial<Pick<ITextObjectOptions, 'value'>>;

export class AtariText extends TextObject<AsteroidsGame> {
  constructor(options: IAtariTextOptions) {
    super('text', { ...options, fontFamily: 'Atari Classic' });
  }
}

export class ScoreText extends AtariText {
  addInteractions(): void {
    this.onUpdate(({ game }) => {
      this.value = `SCORE: ${game.score}`;
    });
  }
}

export class LivesText extends AtariText {
  addInteractions(): void {
    this.onUpdate(({ game }) => {
      this.value = `LIVES: ${game.lives}`;
    });
  }
}

export class LevelText extends AtariText {
  addInteractions(): void {
    this.onUpdate(({ game }) => {
      this.value = `LEVEL: ${game.level}`;
    });
  }
}

export class FpsText extends AtariText {
  addInteractions(): void {
    this.onUpdate(({ game }) => {
      this.value = `FPS: ${game.fps ?? 'implement fps'}`;
    });
  }
}

export class ShieldText extends AtariText {
  ship: Ship;

  constructor({ ship, x, y }: IAtariTextOptions & { ship: Ship }) {
    super({ x, y });
    this.ship = ship;
  }

  addInteractions(): void {
    this.onUpdate(({ game }) => {
      this.value = 'shield';
      if (this.ship.shield) {
        this.value = `SHIELD: ${((this.ship.shieldUntil as number - game.tick) / 1000).toFixed(1)}`;
      } else {
        this.value = null;
      }
    });
  }
}
