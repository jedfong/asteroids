import { IImageObjectOptions, ImageObject, Sprite } from '@yepreally/game-engine';
import debrisImage from 'url:../assets/debris.png';
import { AsteroidsGame } from '../AsteroidsGame';

export class Debris extends ImageObject<AsteroidsGame> {
  constructor(options?: Partial<IImageObjectOptions>) {
    super('debris', { sprite: new Sprite(debrisImage, { width: 1, height: 1 }), speed: 1, ...options });
  }

  addInteractions(): void {
    this.onUpdate(({ screen }) => {
      this.opacity -= 0.01;
      if (this.opacity <= 0) {
        screen.removeObject(this);
      }
    });
  }
}
