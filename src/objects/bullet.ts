import { IImageObjectOptions, ImageObject, Sprite, randomFromRange, repeat } from '@yepreally/game-engine';
import bulletImage from 'url:../assets/bullet.png';
import { AsteroidsGame } from '../AsteroidsGame';
import { Asteroid, largeAsteroidSprite, mediumAsteroidSprite, smallAsteroidSprite } from './asteroid';
import { Debris } from './debris';

export class Bullet extends ImageObject<AsteroidsGame> {
  constructor(options?: Partial<IImageObjectOptions>) {
    super('bullet', {
      sprite: new Sprite(bulletImage, { width: 2, height: 2 }),
      speed: 2.5,
      ...options,
    });
  }

  addInteractions(): void {
    this.onUpdate(({ screen, game }) => {
      if (this.x < 0 || this.x > game.width || this.y < 0 || this.y > game.height) {
        screen.removeObject(this);
      }
    });

    this.onCollisionWith<Asteroid>('asteroid', ({ object: asteroid, screen, game }) => {
      screen.removeObject(this);
      screen.removeObject(asteroid);

      if (asteroid.sprite === largeAsteroidSprite) {
        repeat(2, () => {
          const newAsteroid = new Asteroid({ x: asteroid.x, y: asteroid.y });
          newAsteroid.sprite = mediumAsteroidSprite;
          screen.addObject(newAsteroid);
        });
      } else if (asteroid.sprite === mediumAsteroidSprite) {
        repeat(2, () => {
          const newAsteroid = new Asteroid({ x: asteroid.x, y: asteroid.y });
          newAsteroid.sprite = smallAsteroidSprite;
          screen.addObject(newAsteroid);
        });
      }
      repeat(10, () => {
        screen.addObject(new Debris({ x: asteroid.x, y: asteroid.y, direction: randomFromRange(0, 400) }));
      });
      game.score += 10;

      if (screen.getObjectsByType('asteroid').length === 0) {
        game.nextLevel();
      }
    });
  }
}
