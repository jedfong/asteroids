import { ImageObject, IImageObjectOptions, Sprite } from '@yepreally/game-engine';
import shieldImage from 'url:../assets/shield.png';
import { AsteroidsGame } from '../AsteroidsGame';

export interface IShieldOptions extends Partial<IImageObjectOptions> {
  ship: ImageObject;
}

export class Shield extends ImageObject<AsteroidsGame> {
  ship!: ImageObject;

  constructor({ ship, ...restOptions }: IShieldOptions) {
    super('shield', { sprite: new Sprite(shieldImage, { width: 35, height: 35 }), ...restOptions });
    this.ship = ship;
  }

  addInteractions(): void {
    this.onUpdate(() => {
      this.x = this.ship.x;
      this.y = this.ship.y;
    });
  }
}
