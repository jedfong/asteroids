import { chooseRandom, randomFromRange, ImageObject, IImageObjectOptions, Sprite } from '@yepreally/game-engine';
import asteroidLargeImage from 'url:../assets/asteroid-large.png';
import asteroidMediumImage from 'url:../assets/asteroid-medium.png';
import asteroidSmallImage from 'url:../assets/asteroid-small.png';
import { AsteroidsGame } from '../AsteroidsGame';

export const largeAsteroidSprite = new Sprite(asteroidLargeImage, { height: 32, width: 32 });
export const mediumAsteroidSprite = new Sprite(asteroidMediumImage, { height: 16, width: 16 });
export const smallAsteroidSprite = new Sprite(asteroidSmallImage, { height: 8, width: 8 });

export class Asteroid extends ImageObject<AsteroidsGame> {
  rotationSpeed: number;

  constructor(options: Partial<IImageObjectOptions>) {
    super('asteroid', {
      sprite: chooseRandom(largeAsteroidSprite, mediumAsteroidSprite, smallAsteroidSprite),
      direction: randomFromRange(0, 359),
      rotation: randomFromRange(0, 359),
      screenWrap: true,
      speed: 0.5,
      ...options,
    });
    this.rotationSpeed = randomFromRange(-256, 256) / 100;
  }

  addInteractions(): void {
    this.onUpdate(() => {
      this.rotation += this.rotationSpeed;
    });
  }
}
