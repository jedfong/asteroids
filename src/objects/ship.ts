import {
  keyboardCheck,
  motionAdd,
  ImageObject,
  Sprite,
  IGameObjectOptions,
  keyboardCheckPressed,
  repeat,
  randomFromRange,
  Game,
  Screen,
} from '@yepreally/game-engine';
import shipImage from 'url:../assets/ship.png';
import { AsteroidsGame } from '../AsteroidsGame';
import { Shield } from './shield';
import { Bullet } from './bullet';
import { Debris } from './debris';
import { Asteroid } from './asteroid';

export class Ship extends ImageObject<AsteroidsGame> {
  shieldUntil!: number | null;
  shield!: ImageObject | null;

  constructor(options?: Partial<IGameObjectOptions>) {
    super('ship', { sprite: new Sprite(shipImage), screenWrap: true, ...options });
    this.shieldUntil = null;
    this.shield = null;
  }

  enableShield(screen: Screen, game: Game): void {
    this.shieldUntil = game.tick + 2000;
    this.shield = new Shield({ ship: this });
    screen.addObject(this.shield);
  }

  addInteractions(): void {
    this.onAdd(({ screen, game }) => {
      this.enableShield(screen, game);
    });

    this.onUpdate(({ screen, game }) => {
      if (this.shield && this.shieldUntil && this.shieldUntil <= game.tick) {
        screen.removeObject(this.shield);
        this.shield = null;
      }

      if (keyboardCheck('ArrowRight')) {
        this.rotation += 2;
      }

      if (keyboardCheck('ArrowLeft')) {
        this.rotation -= 2;
      }

      if (keyboardCheck('ArrowUp')) {
        motionAdd(this, this.rotation, 0.02);
      }

      if (keyboardCheckPressed(' ')) {
        screen.addObject(new Bullet({ x: this.x, y: this.y, direction: this.rotation }));
      }
    });

    this.onCollisionWith<Asteroid>('asteroid', ({ game, screen }) => {
      if (this.shield) {
        return;
      }

      game.lives--;
      if (game.lives === 0) {
        screen.removeObject(this);
        repeat(10, () => {
          screen.addObject(new Debris({ x: this.x, y: this.y, direction: randomFromRange(0, 400) }));
        });

        setTimeout(() => {
          game.showEndScreen();
        }, 3000);
      } else {
        this.enableShield(screen, game);
      }
    });
  }
}
