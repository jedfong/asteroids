import '@yepreally/game-engine/dist/styles.css';
import './styles.css';
import { AsteroidsGame } from './AsteroidsGame';

const game = new AsteroidsGame('#game');
game.start();
