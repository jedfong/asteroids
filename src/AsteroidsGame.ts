import { Game, IGameOptions } from '@yepreally/game-engine';
import '@yepreally/game-engine/dist/styles.css';
import './styles.css';
import { PlayScreen } from './screens/playScreen';
import { StartScreen } from './screens/startScreen';
import { EndScreen } from './screens/endScreen';

interface IAsteroidsGameOptions extends IGameOptions {
  lives: number;
  level: number;
}

export class AsteroidsGame extends Game {
  initialLives: number;
  level: number;
  lives: number;
  score: number;
  startScreen: StartScreen;
  playScreen: PlayScreen;
  endScreen: EndScreen;

  constructor(elOrSelector: string | HTMLElement, options?: Partial<IAsteroidsGameOptions>) {
    super(elOrSelector, options);
    this.initialLives = options?.lives ?? 3;
    this.level = options?.level ?? 1;
    this.lives = this.initialLives;
    this.score = 0;
    this.startScreen = new StartScreen();
    this.playScreen = new PlayScreen();
    this.endScreen = new EndScreen();
    this.setScreen(this.startScreen);
  }

  playGame(): void {
    this.score = 0;
    this.lives = this.initialLives;
    this.setScreen(this.playScreen);
  }

  nextLevel(): void {
    this.setScreen(this.playScreen);
    this.level++;
    this.playScreen.loadLevel(this.level);
  }

  showEndScreen(): void {
    this.setScreen(this.endScreen);
  }
}
