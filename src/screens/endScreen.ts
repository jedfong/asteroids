import { Screen, keyboardCheckPressed } from '@yepreally/game-engine';
import { AtariText } from '../objects/text';
import { AsteroidsGame } from '../AsteroidsGame';
// import { AsteroidsGame } from '../AsteroidsGame';
// import { playScreen } from './playScreen';

export class EndScreenText extends AtariText {
  addInteractions(): void {
    this.onUpdate(({ game }) => {
      if (keyboardCheckPressed(' ')) {
        game?.playGame();
      }
    });
  }
}

export class EndScreen extends Screen<AsteroidsGame> {
  gameOverText: EndScreenText | null;
  scoreText: EndScreenText | null;
  playAgainText: EndScreenText | null;

  constructor() {
    super();
    this.gameOverText = null;
    this.scoreText = null;
    this.playAgainText = null;
  }

  addInteractions(): void {
    this.onLoad(({ game }) => {
      this.gameOverText = new AtariText({
        x: (game.width / 2) - 75,
        y: game.height / 2,
        value: 'Game Over',
      });
      this.addObject(this.gameOverText);

      this.gameOverText = new AtariText({
        x: (game.width / 2) - 120,
        y: (game.height / 2) + 25,
        value: `Your Score is ${game.score}.`,
      });
      this.addObject(this.gameOverText);

      this.scoreText = new EndScreenText({
        x: (game.width / 2) - 225,
        y: (game.height / 2) + 50,
        value: 'Press spacebar to play again.',
      });
      this.addObject(this.scoreText);
    });
  }
}
