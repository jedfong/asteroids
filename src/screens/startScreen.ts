import { Screen } from '@yepreally/game-engine';
import { AtariText } from '../objects/text';
import { AsteroidsGame } from '../AsteroidsGame';

export class StartScreen extends Screen<AsteroidsGame> {
  startTime = 0;
  startScreenText?: AtariText | null;

  getTimeRemaining(): number {
    return Math.round((this.startTime - Date.now()) / 1000);
  }

  updateText(timeRemaining: number): void {
    if (this.startScreenText) {
      this.startScreenText.value = `The game will start in ${timeRemaining} seconds`;
    }
  }

  startTimer(durationMs: number): void {
    this.startTime = Date.now() + durationMs;
    this.updateText(this.getTimeRemaining());

    const intervalId = setInterval(() => {
      const timeRemaining = this.getTimeRemaining();
      if (timeRemaining > 0) {
        this.updateText(timeRemaining);
      } else {
        clearInterval(intervalId);
        this.game?.playGame();
      }
    }, 1000);
  }

  addInteractions(): void {
    this.onLoad(() => {
      this.startScreenText = new AtariText({
        x: (this.width / 2) - 250,
        y: this.height / 2,
      });
      this.addObject(this.startScreenText);
      this.startTimer(3000);
    });
  }
}
