import { Screen, randomFromRange, repeat } from '@yepreally/game-engine';
import { Ship } from '../objects/ship';
import { Asteroid } from '../objects/asteroid';
import { FpsText, LevelText, LivesText, ScoreText, ShieldText } from '../objects/text';
import { AsteroidsGame } from '../AsteroidsGame';

export class PlayScreen extends Screen<AsteroidsGame> {
  loadLevel(level: number): void {
    repeat(level + 2, () => {
      this.addObject(new Asteroid({
        x: randomFromRange(0, this.game?.width ?? 0),
        y: randomFromRange(0, this.game?.height ?? 0),
      }));
    });
  }

  addInteractions(): void {
    this.onLoad(({ game }) => {
      const ship = new Ship({ x: game.width / 2, y: game.height / 2 });
      this.addObject(ship);
      this.addObject(new LevelText({ x: 10, y: 25 }));
      this.addObject(new LivesText({ x: 10, y: 50 }));
      this.addObject(new ScoreText({ x: 10, y: 75 }));
      this.addObject(new FpsText({ x: 10, y: 100 }));
      this.addObject(new ShieldText({ ship, x: 10, y: 125 }));

      this.loadLevel(game.level);
    });
  }
}
